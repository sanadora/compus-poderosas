#!/usr/bin/bash

workdir=$(pwd)

function instalar-programas() {
    echo "Instalando los programas necesarios..."
    sudo snap install vlc &&
        sudo snap install zoom-client &&
        sudo snap install emacs --classic

    if [ "$?" = "0" ]; then
        echo "Programas instalados."
    else
        echo "ERROR: no se pudo instalar algun programa."
        exit 1 
    fi
}

function crear-accesos-directos() {
    cd $workdir
    sudo cp google-*.png /usr/share/icons/
    echo "Iconos copiados."

    echo "Creando los accesos directos para MEET y DRIVE..."
    # copiar archivos .desktop al escritorio y a donde los espera el Desktop Environment
    cp google-meet.desktop ~/Escritorio
    cp google-drive.desktop ~/Escritorio

    cd /usr/share/applications/
    sudo ln -s ~/Escritorio/google-meet.desktop
    sudo ln -s ~/Escritorio/google-drive.desktop
    cd ~/Escritorio
    gio set google-meet.desktop metadata::trusted true
    chmod 700 google-meet.desktop
    gio set google-drive.desktop metadata::trusted true
    chmod 700 google-drive.desktop
    
    echo "Accesos directos creados."    
}

function instalar-impresora() {
    cd $workdir
    echo "Instalando drivers para la impresora Epson L395..."
    sudo apt install lsb --yes
    wget 'https://download3.ebz.epson.net/dsc/f/03/00/13/43/81/ba6851ef53476ebdf3ee36600067ce4a3c81c032/epson-inkjet-printer-escpr_1.7.18-1lsb3.2_amd64.deb' --quiet
    sudo dpkg -i epson-inkjet-printer*
    echo 'IMPRESORA: con la impresora enchufada por USB, apretar la tecla de "windows", escribir e ir a "impresoras", y agrega una nueva impresora.'
    echo '           Hacé una "impresión de prueba" para corroborar que funciona bien.'
}

instalar-programas && crear-accesos-directos
instalar-impresora

